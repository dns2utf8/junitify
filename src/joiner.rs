//     junitify - Takes cargo test JSON and transform to JUnit XML
//
//         The MIT License (MIT)
//
//      Copyright (c) KoresFramework (https://gitlab.com/Kores/)
//      Copyright (c) contributors
//
//      Permission is hereby granted, free of charge, to any person obtaining a copy
//      of this software and associated documentation files (the "Software"), to deal
//      in the Software without restriction, including without limitation the rights
//      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//      copies of the Software, and to permit persons to whom the Software is
//      furnished to do so, subject to the following conditions:
//
//      The above copyright notice and this permission notice shall be included in
//      all copies or substantial portions of the Software.
//
//      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//      THE SOFTWARE.
use colored::ColoredString;

pub(crate) struct StringJoiner {
    prefix: Option<String>,
    delimiter: Option<String>,
    postfix: Option<String>,
    content: String,
    elements: usize,
}

impl StringJoiner {
    #[allow(dead_code)]
    pub(crate) fn new(
        prefix: Option<String>,
        delimiter: Option<String>,
        postfix: Option<String>,
    ) -> Self {
        Self {
            prefix,
            delimiter,
            postfix,
            content: String::default(),
            elements: 0,
        }
    }

    pub(crate) fn new_colored(
        prefix: Option<ColoredString>,
        delimiter: Option<ColoredString>,
        postfix: Option<ColoredString>,
    ) -> Self {
        Self {
            prefix: prefix.map(|prefix| format!("{}", prefix)),
            delimiter: delimiter.map(|delimiter| format!("{}", delimiter)),
            postfix: postfix.map(|postfix| format!("{}", postfix)),
            content: String::default(),
            elements: 0,
        }
    }

    pub(crate) fn append_str<'a, S>(mut self, str: S) -> Self
    where
        S: Into<&'a str>,
    {
        if self.elements > 0 {
            if let Some(ref delimiter) = self.delimiter {
                self.content.push_str(delimiter);
            }
        } else {
            if let Some(ref prefix) = self.prefix {
                self.content.push_str(prefix)
            }
        }
        self.content.push_str(str.into());
        self.elements += 1;
        self
    }

    #[allow(dead_code)]
    pub(crate) fn append_string<S>(self, str: S) -> Self
    where
        S: Into<String>,
    {
        self.append_str(str.into().as_str())
    }

    pub(crate) fn append_colored_string<S>(self, str: S) -> Self
    where
        S: Into<ColoredString>,
    {
        self.append_str(format!("{}", str.into()).as_str())
    }

    pub(crate) fn option_append_colored_string<S>(self, str: Option<S>) -> Self
    where
        S: Into<ColoredString>,
    {
        if let Some(str) = str {
            self.append_colored_string(str)
        } else {
            self
        }
    }
}

impl From<StringJoiner> for String {
    fn from(joiner: StringJoiner) -> Self {
        if let Some(ref postfix) = joiner.postfix {
            format!("{}{}", joiner.content, postfix)
        } else {
            joiner.content
        }
    }
}
